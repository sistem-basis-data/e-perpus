// Disable char e, +, - on input element with type number

const inputNumber = document.querySelectorAll('input[type="number"]');

if (inputNumber) {
	inputNumber.forEach((iN) => {
		iN.addEventListener("keypress", (event) => {
			if (
				(event.which != 8 && event.which != 0 && event.which < 48) ||
				event.which > 57
			) {
				event.preventDefault();
			}
		});
	});
}

// Preview the image uploaded
function previewImage() {
	const gambar = document.querySelector(".gambar-preview");
	const imgPreview = document.querySelector(".img-preview");
	// console.log(imgPreview);

	const oFReader = new FileReader();
	oFReader.readAsDataURL(gambar.files[0]);

	oFReader.onload = function (oFREvent) {
		imgPreview.src = oFREvent.target.result;
	};
}

const keluar = document.querySelector("#keluar");
keluar.addEventListener("click", function () {
	const dataUrl = keluar.dataset.url;
	Swal.fire({
		icon: "warning",
		html: `Apakah anda yakin ingin keluar?`,
		showCancelButton: true,
		confirmButtonColor: "#d9534f",
		cancelButtonColor: "#5cb85c",
		confirmButtonText: `Ya`,
		cancelButtonText: `Tidak`,
	}).then((result) => {
		if (result.isConfirmed) {
			location.href = `${dataUrl}`;
		}
	});
});

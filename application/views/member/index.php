        <div class="page-content">
            <section class="row">
                <div class="col-12 col-lg-9">
                    <div class="row">
                        <div class="col-6 col-lg-3 col-md-6">
                            <div class="card">
                                <div class="card-body px-4 py-4-5">
                                    <div class="row">
                                        <div class="col-md-4 col-lg-12 col-xl-12 col-xxl-5 d-flex justify-content-start ">
                                            <div class="stats-icon purple mb-2">
                                                <i class="iconly-boldUser1"></i>
                                            </div>
                                        </div>
                                        <div class="col-md-8 col-lg-12 col-xl-12 col-xxl-7">
                                            <h6 class="text-muted font-semibold">Total Buku Dipinjam</h6>
                                            <h6 class="font-extrabold mb-0"><?= $total_buku_dipinjam ? $total_buku_dipinjam : 0; ?></h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-6 col-lg-3 col-md-6">
                            <div class="card">
                                <div class="card-body px-4 py-4-5">
                                    <div class="row">
                                        <div class="col-md-4 col-lg-12 col-xl-12 col-xxl-5 d-flex justify-content-start ">
                                            <div class="stats-icon blue mb-2">
                                                <i class="iconly-boldPassword"></i>
                                            </div>
                                        </div>
                                        <div class="col-md-8 col-lg-12 col-xl-12 col-xxl-7">
                                            <h6 class="text-muted font-semibold">Total Buku Dikembalikan</h6>
                                            <h6 class="font-extrabold mb-0"><?= $total_buku_dikembalikan ? $total_buku_dikembalikan : 0; ?></h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-6 col-lg-3 col-md-6">
                            <div class="card">
                                <div class="card-body px-4 py-4-5">
                                    <div class="row">
                                        <div class="col-md-4 col-lg-12 col-xl-12 col-xxl-5 d-flex justify-content-start ">
                                            <div class="stats-icon green mb-2">
                                                <i class="iconly-boldAdd-User"></i>
                                            </div>
                                        </div>
                                        <div class="col-md-8 col-lg-12 col-xl-12 col-xxl-7">
                                            <h6 class="text-muted font-semibold">Total Transaksi</h6>
                                            <h6 class="font-extrabold mb-0"><?= $total_transaksi ? $total_transaksi : 0; ?></h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-6 col-lg-3 col-md-6">
                            <div class="card">
                                <div class="card-body px-4 py-4-5">
                                    <div class="row">
                                        <div class="col-md-4 col-lg-12 col-xl-12 col-xxl-5 d-flex justify-content-start ">
                                            <div class="stats-icon red mb-2">
                                                <i class="iconly-boldWork"></i>
                                            </div>
                                        </div>
                                        <div class="col-md-8 col-lg-12 col-xl-12 col-xxl-7">
                                            <h6 class="text-muted font-semibold">Total Denda</h6>
                                            <h6 class="font-extrabold mb-0">Rp. <?= number_format($total_denda, 0, ',', '.'); ?></h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Riwayat Bulanan Buku Yang Dipinjam</h4>
                                </div>
                                <div class="card-body">
                                    <div id="chart-bulanan-meminjam-buku"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-3">
                    <div class="card">
                        <div class="card-body py-4 px-4">
                            <div class="d-flex align-items-center">
                                <div class="avatar avatar-xl">
                                    <img src="<?= base_url(); ?>/assets/img/avatar_image/<?= $user['avatar_image']; ?>" alt="Avatar Image">
                                </div>
                                <div class="ms-3 name">
                                    <h5 class="font-bold"><?= $user['name']; ?></h5>
                                    <h6 class="text-muted mb-0"><?= $user['email']; ?></h6>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header">
                            <h4>Buku Yang Baru Dipinjam</h4>
                        </div>
                        <div class="card-content pb-4">
                            <?php foreach ($data_buku_yang_baru_dipinjam as $buku) : ?>
                                <div class="recent-message d-flex px-4 py-3">
                                    <div class="avatar avatar-lg">
                                        <img src="<?= base_url(); ?>assets/img/cover_image/<?= $buku['cover_image'] ?>">
                                    </div>
                                    <div class="name ms-4">
                                        <h5 class="mb-1"><?= $buku['title']; ?></h5>
                                        <h6 class="text-muted mb-0"><?= $buku['author']; ?></h6>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </section>
        </div>
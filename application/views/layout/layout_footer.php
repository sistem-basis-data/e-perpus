</div>

<footer class="main-footer">
    <div class="footer clearfix mb-0 text-muted">
        <div class="float-start">
            <p>&copy; Copyright 2023 - <?= date("Y"); ?> <a href="https://gitlab.com/sistem-basis-data/e-perpus" target="_blank">e-perpus</a>. All rights reserved.</p>
        </div>
        <div class="float-end">
            <p>
                Developed with <span class="text-danger"><i class="bi bi-heart-fill icon-mid"></i></span> By
                Adi,
                <a href="https://gitlab.com/armandwipangestu" target="_blank">Arman</a>,
                Diaz,
                Vito
            </p>
        </div>
    </div>
</footer>


<script src="<?= base_url(); ?>template/mazer/dist/assets/static/js/components/dark.js"></script>
<script src="<?= base_url(); ?>template/mazer/dist/assets/extensions/perfect-scrollbar/perfect-scrollbar.min.js"></script>

<script src="<?= base_url(); ?>template/mazer/dist/assets/compiled/js/app.js"></script>

<!-- Need: Apexcharts -->
<script src="<?= base_url(); ?>template/mazer/dist/assets/extensions/apexcharts/apexcharts.min.js"></script>
<script src="<?= base_url(); ?>template/mazer/dist/assets/static/js/pages/dashboard.js"></script>

<script src="<?= base_url(); ?>template/mazer/dist/assets/extensions/sweetalert2/sweetalert2.min.js"></script>
<script src="<?= base_url(); ?>template/mazer/dist/assets/static/js/pages/sweetalert2.js"></script>

<!-- Custom JS -->
<script src="<?= base_url(); ?>assets/js/script.js"></script>

<script>
    var getDataGenderUser = <?= $data_gender_user; ?>;

    var seriesData = getDataGenderUser.map(function(item) {
        return parseInt(item.total);
    });

    var optionsGender = {
        series: seriesData,
        labels: ["Laki-Laki", "Perempuan"],
        colors: ["#435ebe", "#55c6e8"],
        chart: {
            type: "donut",
            width: "100%",
            height: "350px",
        },
        legend: {
            position: "bottom",
        },
        plotOptions: {
            pie: {
                donut: {
                    size: "30%",
                },
            },
        },
    };

    var chartGender = new ApexCharts(
        document.getElementById("chart-gender"),
        optionsGender
    );

    chartGender.render();
</script>

<script>
    var getDataUserDaftar = <?= $data_user_daftar; ?>;

    var optionsUserDaftar = {
        annotations: {
            position: "back",
        },
        dataLabels: {
            enabled: false,
        },
        chart: {
            type: "bar",
            height: 300,
        },
        fill: {
            opacity: 1,
        },
        plotOptions: {},
        series: [{
            name: "User Daftar",
            data: getDataUserDaftar.map(item => {
                return Number(item.total)
            }),
        }, ],
        colors: "#435ebe",
        xaxis: {
            categories: getDataUserDaftar.map(item => item.month),
        },
    };

    var chartUserDaftar = new ApexCharts(
        document.querySelector("#chart-user-daftar"),
        optionsUserDaftar
    );

    chartUserDaftar.render();
</script>

<script>
    var getDataBulananMeminjamBuku = <?= $data_bulanan_meminjam_buku; ?>;

    var optionsBulananMeminjamBuku = {
        annotations: {
            position: "back",
        },
        dataLabels: {
            enabled: false,
        },
        chart: {
            type: "bar",
            height: 300,
        },
        fill: {
            opacity: 1,
        },
        plotOptions: {},
        series: [{
            name: "Data Bulanan Meminjam Buku",
            data: getDataBulananMeminjamBuku.map(item => {
                return Number(item.total)
            }),
        }, ],
        colors: "#435ebe",
        xaxis: {
            categories: getDataBulananMeminjamBuku.map(item => item.month),
        },
    };

    var chartBulananMeminjamBuku = new ApexCharts(
        document.querySelector("#chart-bulanan-meminjam-buku"),
        optionsBulananMeminjamBuku
    );

    chartBulananMeminjamBuku.render();
</script>

</body>

</html>
<div class="flex">
    <div class="float-end">
        <label class="form-label">Terakhir diubah: <?= bulan_indonesia(strtotime($user['updated_at'])); ?></label>
    </div>
</div>

<section class="section">
    <?= form_open_multipart('user/ubah'); ?>
    <div class="row">
        <div class="col-12 col-lg-4">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex justify-content-center align-items-center flex-column">
                        <div class="avatar avatar-2xl" onclick="chooseFile()">
                            <img src="<?= base_url(); ?>assets/img/avatar_image/<?= $user['avatar_image'] ?>" alt="Avatar Image" class="cursor-pointer hover-scale img-preview">
                            <input type="file" id="avatar_image" style="display:none;" name="avatar_image" onchange="previewImage()" class="gambar-preview">
                        </div>
                        <ul class="text-small text-muted mt-3">
                            <li>Maks upload file: 2MB</li>
                            <li>Ekstensi yang diperbolehkan: .jpg dan .png</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 col-lg-8">
            <div class="card">
                <div class="card-body">
                    <div class="form-group">
                        <label for="name" class="form-label">Nama</label>
                        <input type="text" name="name" id="name" class="form-control" value="<?= $user['name']; ?>">
                        <?= form_error('name', '<label for="name" class="form-label text-danger mt-2">', '</label>'); ?>
                    </div>
                    <div class="form-group">
                        <label for="email" class="form-label">Email</label>
                        <input type="email" name="email" id="email" class="form-control" value="<?= $user['email']; ?>" disabled>
                        <?= form_error('email', '<label for="email" class="form-label text-danger mt-2">', '</label>'); ?>
                    </div>
                    <div class="form-group">
                        <label for="phone_number" class="form-label">No Telepon</label>
                        <input type="number" name="phone_number" id="phone_number" class="form-control" value="<?= $user['phone_number']; ?>">
                        <?= form_error('number', '<label for="number" class="form-label text-danger mt-2">', '</label>'); ?>
                    </div>
                    <div class="form-group">
                        <label for="address" class="form-label">Alamat</label>
                        <textarea id="address" name="address" class="form-control" style="height: 100px !important;"><?= $user['address']; ?></textarea>
                        <?= form_error('address', '<label for="address" class="form-label text-danger mt-2">', '</label>'); ?>
                    </div>

                    <fieldset class="form-group">
                        <label for="gender" class="form-label">Jenis Kelamin</label>
                        <select name="gender" id="gender" class="form-select form-control" id="basicSelect">
                            <option value="L" <?= isset($user['gender']) && $user['gender'] == "L" ? 'selected' : ''; ?>>Laki-Laki</option>
                            <option value="P" <?= isset($user['gender']) && $user['gender'] == "P" ? 'selected' : ''; ?>>Perempuan</option>
                        </select>
                        <?= form_error('gender', '<label for="gender" class="form-label text-danger mt-2">', '</label>'); ?>
                    </fieldset>

                    <div class="form-group float-start">
                        <button type="submit" class="btn btn-success">Simpan</button>
                    </div>

                    <div class="form-group float-end">
                        <a href="#" id="hapus_akun" class="btn btn-danger">Hapus Akun</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</section>

<script>
    function chooseFile() {
        document.getElementById('avatar_image').click();
    }

    document.getElementById("hapus_akun").addEventListener("click", async (e) => {
        const {
            value: confirm
        } = await Swal2.fire({
            icon: "warning",
            title: "Konfirmasi Menghapus Akun!",
            input: "text",
            inputLabel: "Ketik 'Ya Saya Yakin'",
            inputPlaceholder: "Ya Saya Yakin",
            html: "Akun yang dihapus tidak dapat dikembalikan!",
            showCancelButton: true,
            cancelButtonText: `Tidak`,
            inputValidator: (value) => {
                return new Promise((resolve) => {
                    if (value === "Ya Saya Yakin") {
                        resolve()
                    } else {
                        resolve("Kamu harus mengetik Ya Saya Yakin")
                    }
                })
            },
        })

        if (confirm) {
            window.location.href = "<?= base_url('user/hapus_akun'); ?>";
        }
    })
</script>
<?= $this->session->flashdata('message') ?>

<section class="section">
    <div class="row">
        <div class="col-12 col-lg-4">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex justify-content-center align-items-center flex-column">
                        <div class="avatar avatar-2xl">
                            <img src="<?= base_url(); ?>assets/img/avatar_image/<?= $user['avatar_image'] ?>" alt="Avatar Image">
                        </div>

                        <h3 class="mt-3"><?= $user['name']; ?></h3>
                        <p class="text-small"><?= $role_name; ?> - Sejak <?= bulan_indonesia(strtotime($user['created_at'])); ?></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-8">
            <div class="card">
                <div class="card-body">
                    <form action="#" method="get">
                        <div class="form-group">
                            <label for="name" class="form-label">Nama</label>
                            <input type="text" name="name" id="name" class="form-control" value="<?= $user['name']; ?>" disabled>
                        </div>
                        <div class="form-group">
                            <label for="email" class="form-label">Email</label>
                            <input type="email" name="email" id="email" class="form-control" value="<?= $user['email']; ?>" disabled>
                        </div>
                        <div class="form-group">
                            <label for="phone_number" class="form-label">No Telepon</label>
                            <input type="number" name="phone_number" id="phone_number" class="form-control" value="<?= $user['phone_number']; ?>" disabled>
                        </div>
                        <div class="form-group">
                            <label for="address" class="form-label">Alamat</label>
                            <textarea id="address" name="address" class="form-control" style="height: 100px !important;" disabled><?= $user['address']; ?></textarea>

                        </div>
                        <div class="form-group">
                            <label for="gender" class="form-label">Jenis Kelamin</label>
                            <input type="text" name="gender" id="gender" class="form-control" value="<?= $user['gender'] == "L" ? 'Laki-Laki' : 'Perempuan'; ?>" disabled>
                        </div>
                        <div class="form-group">
                            <a href="<?= base_url("user/ubah"); ?>" class="btn btn-primary">Ubah Profil</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        _checkIsLogin();
        $this->load->model('User_model', 'user');
    }

    public function index()
    {
        $data['title'] = 'Profil Saya';
        $data['user'] = $this->db->get_where('user_data', ['email' => $this->session->userdata('email')])->row_array();
        $data['role_name'] = $this->user->getRole($data['user']['role_id'])['role'];

        $this->load->view('layout/layout_header', $data);
        $this->load->view('layout/layout_topbar');
        $this->load->view('layout/layout_sidebar');
        $this->load->view('user/index', $data);
        $this->load->view('layout/layout_footer');
    }

    public function ubah()
    {
        $data['title'] = 'Ubah Profil';
        $data['user'] = $this->db->get_where('user_data', ['email' => $this->session->userdata('email')])->row_array();
        $data['role_name'] = $this->user->getRole($data['user']['role_id'])['role'];

        $this->form_validation->set_rules('name', 'Nama', 'required|trim', ['required' => 'Nama tidak boleh kosong']);
        $this->form_validation->set_rules('gender', 'Jenis Kelamin', 'required|trim', ['required' => 'Jenis Kelamin tidak boleh kosong']);
        $this->form_validation->set_rules('address', 'Alamat', 'required|trim', ['required' => 'Alamat tidak boleh kosong']);
        $this->form_validation->set_rules('phone_number', 'No Telepon', 'required|trim', ['required' => 'No Telepon tidak boleh kosong']);

        if ($this->form_validation->run() == false) {
            $this->load->view('layout/layout_header', $data);
            $this->load->view('layout/layout_topbar');
            $this->load->view('layout/layout_sidebar');
            $this->load->view('user/ubah');
            $this->load->view('layout/layout_footer');
        } else {
            $data = [
                'name' => htmlspecialchars($this->input->post('name', true)),
                'gender' => htmlspecialchars($this->input->post('gender', true)),
                'address' => htmlspecialchars($this->input->post('address', true)),
                'phone_number' => htmlspecialchars($this->input->post('phone_number', true)),
            ];

            // cek jika ada gambar yang akan diupload
            $upload_image = $_FILES['avatar_image']['name'];

            if ($upload_image) {
                $config['allowed_types'] = 'jpg|png';
                $config['max_size'] = '2048';
                $config['upload_path'] = './assets/img/avatar_image/';

                // Mendapatkan ekstensi file
                $file_ext = pathinfo($_FILES['avatar_image']['name'], PATHINFO_EXTENSION);

                // Membuat nama file unik dengan menggunakan uniqid() dan tambahan ekstensi
                $unique_filename = uniqid() . '_' . time() . '.' . $file_ext;

                $config['file_name'] = $unique_filename;

                $this->load->library('upload', $config);

                if ($this->upload->do_upload('avatar_image')) {
                    $gambar_lama = $this->db->get_where("user_data", ['id' => $this->session->userdata('id_user')])->row_array()['avatar_image'];
                    if ($gambar_lama != "default_laki_laki.png" && $gambar_lama != "default_perempuan.png") {
                        unlink(FCPATH . 'assets/img/avatar_image/' . $gambar_lama);
                    }
                    $gambar_baru = $this->upload->data('file_name');
                    $this->db->set('avatar_image', $gambar_baru);
                } else {
                    echo $this->upload->display_errors();
                }
            }

            $this->db->where('id', $this->session->userdata('id_user'));
            $this->db->update('user_data', $data);

            $this->session->set_flashdata(
                'message',
                '<div class="alert alert-success mb-4">Profil berhasil disimpan!</div>'
            );
            redirect('user');
        }
    }

    public function hapus_akun()
    {
        $gambar_avatar = $this->db->get_where('user_data', ['id' => $this->session->userdata('id_user')])->row_array()['avatar_image'];

        if ($gambar_avatar != "default_laki_laki.png" && $gambar_avatar != "default_perempuan.png") {
            unlink(FCPATH . 'assets/img/avatar_image/' . $gambar_avatar);
        }

        $this->db->delete('user_data', ['id' => $this->session->userdata('id_user')]);

        $this->session->set_flashdata('message', '<div class="alert alert-success ml-4 mr-4">Akun anda berhasil dihapus!</div>');
        redirect('auth/logout');
    }

    public function ganti_kata_sandi()
    {
        $data['title'] = 'Ganti Kata Sandi';
        $data['user'] = $this->db->get_where('user_data', ['email' => $this->session->userdata('email')])->row_array();

        $this->form_validation->set_rules('current_password', 'Kata Sandi', 'required|trim', [
            'required' => 'Tidak boleh kosong',
            'min_length' => 'Terlalu pendek'
        ]);

        $this->form_validation->set_rules('password1', 'Kata Sandi Baru', 'required|trim|min_length[3]|matches[password2]', [
            'required' => 'Tidak boleh kosong',
            'matches' => '',
            'min_length' => 'Terlalu pendek'
        ]);

        $this->form_validation->set_rules('password2', 'Konfirmasi Kata Sandi Baru', 'required|trim|matches[password1]', [
            'required' => 'Tidak boleh kosong',
            'matches' => 'Tidak sama dengan kata sandi baru',
        ]);

        if ($this->form_validation->run() == false) {
            $this->load->view('layout/layout_header', $data);
            $this->load->view('layout/layout_topbar');
            $this->load->view('layout/layout_sidebar');
            $this->load->view('user/ganti_kata_sandi', $data);
            $this->load->view('layout/layout_footer');
        } else {
            $current_password = $this->input->post('current_password');
            $new_password = $this->input->post('password1');
            if (!password_verify($current_password, $data['user']['password'])) {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">Kata sandi yang anda masukan salah!</div>');
                redirect('user/ganti_kata_sandi');
            } else {
                if ($current_password == $new_password) {
                    $this->session->set_flashdata('message', '<div class="alert alert-warning">Kata sandi baru tidak bisa boleh sama dengan kata sandi saat ini!</div>');
                    redirect('user/ganti_kata_sandi');
                } else {
                    // password sudah ok
                    $password_hash = password_hash($new_password, PASSWORD_DEFAULT);

                    $this->db->set('password', $password_hash);
                    $this->db->where('email', $this->session->userdata('email'));
                    $this->db->update('user_data');

                    $this->session->set_flashdata(
                        'message',
                        '<div class="alert alert-success">Kata sandi berhasil diubah!</div>'
                    );
                    redirect('user/ganti_kata_sandi');
                }
            }
        }
    }
}

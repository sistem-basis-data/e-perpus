<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Member extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        _checkIsLogin();
        $this->load->model('Member_model', 'member');
    }

    public function index()
    {
        $data['title'] = 'Dashboard';
        $data['user'] = $this->db->get_where('user_data', ['email' => $this->session->userdata('email')])->row_array();

        $data['total_buku_dipinjam'] = $this->member->getTotalBukuDipinjam($data['user']['id']);
        $data['total_buku_dikembalikan'] = $this->member->getTotalBukuDikembalikan($data['user']['id']);
        $data['total_transaksi'] = $this->member->getTotalTransaksi($data['user']['id']);
        $data['total_denda'] = $this->member->getTotalDenda($data['user']['id']);
        $data['data_bulanan_meminjam_buku'] = json_encode($this->member->getDataBulananMeminjamBuku($data['user']['id']));
        $data['data_buku_yang_baru_dipinjam'] = $this->member->getBukuYangBaruDipinjam($data['user']['id']);

        $this->load->view('layout/layout_header', $data);
        $this->load->view('layout/layout_topbar');
        $this->load->view('layout/layout_sidebar');
        $this->load->view('member/index');
        $this->load->view('layout/layout_footer');
    }
}

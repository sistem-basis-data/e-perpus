<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
    private $data = array();

    public function __construct()
    {
        parent::__construct();
        $this->load->library("form_validation");
    }

    public function index()
    {
        $this->_alreadyLogin();
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email', [
            "required" => "Email tidak boleh kosong",
            'valid_email' => 'Email yang anda masukan tidak valid',
        ]);
        $this->form_validation->set_rules('password', 'Password', 'trim|required', [
            "required" => "Password tidak boleh kosong",
        ]);

        if ($this->form_validation->run() == FALSE) {
            $data['title'] = "E-PERPUS - Masuk";
            $this->load->view('layout/layout_header', $data);
            $this->load->view('auth/login');
            $this->load->view('layout/layout_footer_auth');
        } else {
            $this->_masuk();
        }
    }

    private function _masuk()
    {
        $email = htmlspecialchars($this->input->post('email'), true);
        $password = $this->input->post('password');

        $user = $this->db->get_where('user_data', ['email' => $email])->row_array();
        if ($user) {
            if (password_verify($password, $user['password'])) {
                $data = [
                    'id_user' => $user['id'],
                    'email' => $user['email'],
                    'role_id' => $user['role_id']
                ];
                $this->session->set_userdata($data);
                if ($user['role_id'] == 1) {
                    redirect('admin');
                }
                if ($user['role_id'] == 2) {
                    redirect('member');
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger ml-4 mr-4">Kata sandi yang anda masukan salah</div>');
                redirect('auth');
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger ml-4 mr-4">Email yang anda masukan belum terdaftar</div>');
            redirect('auth');
        }
    }

    private function _alreadyLogin()
    {
        if ($this->session->userdata('role_id') == 1) {
            redirect('admin');
        }

        if ($this->session->userdata('role_id') == 2) {
            redirect('member');
        }
    }

    public function daftar()
    {
        $this->_alreadyLogin();
        $this->form_validation->set_rules('name', 'Nama', 'required|trim', ['required' => 'Nama tidak boleh kosong']);
        $this->form_validation->set_rules('gender', 'Jenis Kelamin', 'required|trim|callback_validate_gender', ['required' => 'Jenis Kelamin tidak boleh kosong']);
        $this->form_validation->set_rules('address', 'Alamat', 'required|trim', ['required' => 'Alamat tidak boleh kosong']);
        $this->form_validation->set_rules('phone_number', 'No Telepon', 'required|trim', ['required' => 'No Telepon tidak boleh kosong']);
        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[user_data.email]', [
            'required' => 'Email tidak boleh kosong',
            'valid_email' => 'Email yang anda masukan tidak valid',
            'is_unique' => 'Email yang anda masukan sudah terdaftar, gunakan email lain untuk mendaftar'
        ]);
        $this->form_validation->set_rules('password1', 'Kata Sandi', 'required|trim|min_length[3]|matches[password2]', [
            'required' => 'Kata Sandi tidak boleh kosong',
            'matches' => '',
            'min_length' => 'Kata Sandi terlalu pendek'
        ]);
        $this->form_validation->set_rules('password2', 'Konfirmasi Kata Sandi', 'required|trim|matches[password1]', [
            'required' => 'Konfirmasi Kata Sandi tidak boleh kosong',
            'matches' => 'Konfirmasi Kata Sandi tidak sama dengan Kata Sandi',
        ]);

        if ($this->form_validation->run() == FALSE) {
            $this->data['title'] = "E-PERPUS - Daftar";
            $this->load->view('layout/layout_header', $this->data);
            $this->load->view('auth/daftar');
            $this->load->view('layout/layout_footer_auth');
        } else {
            $avatar_image = htmlspecialchars($this->input->post('gender', true)) == "L" ? "default_laki_laki.png" : "default_perempuan.png";
            $data = [
                'name' => htmlspecialchars($this->input->post('name', true)),
                'email' => htmlspecialchars($this->input->post('email', true)),
                'password' => password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
                'gender' => htmlspecialchars($this->input->post('gender', true)),
                'address' => htmlspecialchars($this->input->post('address', true)),
                'phone_number' => htmlspecialchars($this->input->post('phone_number', true)),
                'avatar_image' => $avatar_image,
                'role_id' => 2,
            ];

            $this->db->insert('user_data', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success ml-4 mr-4">Akun berhasil dibuat! Silahkan Masuk</div>');
            redirect('auth');
        }
    }

    public function validate_gender($input)
    {
        if ($input == "") {
            $this->form_validation->set_message('validate_gender', 'Jenis Kelamin tidak boleh kosong');
            return false;
        } else {
            $this->data['selected_gender'] = $input;
            return true;
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('id_user');
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('role_id');

        if ($this->session->flashdata('message')) {
            $this->session->set_flashdata('message', '<div class="alert alert-success ml-4 mr-4">Akun anda berhasil dihapus!</div>');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-success ml-4 mr-4">Anda telah keluar!</div>');
        }
        redirect('auth');
    }

    public function blocked()
    {
        $data['title'] = 'ERROR';
        $data['error_code'] = "403";
        $data['error_message'] = "You are unauthorized to see this page.";
        $data['user'] = $this->db->get_where('user_data', ['email' => $this->session->userdata('email')])->row_array();

        $this->load->view('layout/layout_header', $data);
        $this->load->view('layout/layout_topbar', $data);
        $this->load->view('layout/layout_sidebar', $data);
        $this->load->view('auth/blocked', $data);
        $this->load->view('layout/layout_footer');
    }
}

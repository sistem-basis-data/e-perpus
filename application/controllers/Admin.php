<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        _checkIsLogin();
        $this->load->model('Admin_model', 'admin');
    }

    public function index()
    {
        $data['title'] = 'Dashboard';
        $data['user'] = $this->db->get_where('user_data', ['email' => $this->session->userdata('email')])->row_array();

        $data['total_akun'] = $this->db->from('user_data')->count_all_results();
        $data['total_admin'] = $this->admin->getTotalAdmin("Administrator")['total'];
        $data['total_member'] = $this->admin->getTotalAdmin("Member")['total'];
        $data['total_role'] = $this->db->from('user_role')->count_all_results();
        $data['data_gender_user'] = json_encode($this->admin->getDataGenderUser());
        $data['data_user_daftar'] = json_encode($this->admin->getDataUserDaftar());

        $this->load->view('layout/layout_header', $data);
        $this->load->view('layout/layout_topbar');
        $this->load->view('layout/layout_sidebar');
        $this->load->view('admin/index');
        $this->load->view('layout/layout_footer');
    }
}

<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_model extends CI_Model
{
    public function getTotalAdmin($role)
    {
        $role = $this->db->escape($role);

        $query = $this->db->query("SELECT
	            COUNT(*) AS total
                FROM user_data AS ud
                JOIN user_role AS ur
                ON ud.role_id = ur.id
                WHERE ur.role = $role")->row_array();

        return $query;
    }

    public function getDataGenderUser()
    {
        // Sesuaikan dengan query untuk mengambil data gender dari tabel user
        $query = $this->db->query("SELECT gender, COUNT(*) as total FROM user_data GROUP BY gender");
        return $query->result_array();
    }

    public function getDataUserDaftar()
    {
        $query = $this->db->query("SELECT DATE_FORMAT(created_at, '%b') as month, COUNT(*) as total FROM user_data GROUP BY MONTH(created_at), created_at;");

        // Ambil hasil query sebagai array
        $result = $query->result_array();

        // Inisialisasi array asosiatif untuk menyimpan data yang telah dielaborasi
        $processedData = array();

        // Proses data untuk menggabungkan total jika bulan sama
        foreach ($result as $row) {
            $month = $row['month'];
            $total = $row['total'];

            // Jika bulan sudah ada dalam array processedData, tambahkan totalnya
            if (isset($processedData[$month])) {
                $processedData[$month]['total'] += $total;
            } else {
                // Jika bulan belum ada dalam array processedData, tambahkan data baru
                $processedData[$month] = array(
                    'month' => $month,
                    'total' => $total
                );
            }
        }

        // Ubah array asosiatif ke dalam array numerik
        $processedData = array_values($processedData);

        return $processedData;
    }
}

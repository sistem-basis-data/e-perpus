<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Member_model extends CI_Model
{
    public function getTotalBukuDipinjam($user_id)
    {
        $this->db->select_sum('quantity');
        $this->db->where('user_id', $user_id);
        $result = $this->db->get('transaction_borrow')->row_array();

        return $result['quantity'];
    }

    public function getTotalTransaksi($user_id)
    {
        $this->db->where('user_id', $user_id);
        $result = $this->db->count_all_results('transaction_borrow');

        return $result;
    }

    public function getTotalBukuDikembalikan($user_id)
    {
        // $this->db->select('COUNT(DISTINCT tr.borrow_id) as total_buku_dikembalikan');
        $this->db->select_sum('tr.quantity');
        $this->db->from('transaction_return tr');
        $this->db->join('transaction_borrow tb', 'tr.borrow_id = tb.id', 'inner');
        $this->db->where('tb.user_id', $user_id);
        $result = $this->db->get()->row_array();

        return $result['quantity'];
    }

    public function getTotalDenda($user_id)
    {
        $this->db->select_sum('fine_amount');
        $this->db->from('transaction_return');
        $this->db->join('transaction_borrow', 'transaction_return.borrow_id = transaction_borrow.id', 'inner');
        $this->db->where('transaction_borrow.user_id', $user_id);

        $result = $this->db->get()->row_array();

        return $result['fine_amount'];
    }

    public function getDataBulananMeminjamBuku($user_id)
    {
        // $query = $this->db->query("SELECT DATE_FORMAT(borrow_date, '%b') as month, COUNT(*) as total FROM transaction_borrow GROUP BY MONTH(borrow_date), borrow_date;");
        $query = $this->db->query("
        SELECT DATE_FORMAT(tb.borrow_date, '%b') as month, COUNT(*) as total
        FROM transaction_borrow tb
        WHERE tb.user_id = $user_id
        GROUP BY MONTH(tb.borrow_date), tb.borrow_date;
    ");

        // Ambil hasil query sebagai array
        $result = $query->result_array();

        // Inisialisasi array asosiatif untuk menyimpan data yang telah dielaborasi
        $processedData = array();

        // Proses data untuk menggabungkan total jika bulan sama
        foreach ($result as $row) {
            $month = $row['month'];
            $total = $row['total'];

            // Jika bulan sudah ada dalam array processedData, tambahkan totalnya
            if (isset($processedData[$month])) {
                $processedData[$month]['total'] += $total;
            } else {
                // Jika bulan belum ada dalam array processedData, tambahkan data baru
                $processedData[$month] = array(
                    'month' => $month,
                    'total' => $total
                );
            }
        }

        // Ubah array asosiatif ke dalam array numerik
        $processedData = array_values($processedData);

        return $processedData;
    }

    public function getBukuYangBaruDipinjam($user_id)
    {
        $query = $this->db->query("
        SELECT bd.id, bd.title, ba.author, bd.cover_image, MAX(tb.borrow_date) as borrow_date
        FROM transaction_borrow tb
        JOIN book_data bd ON tb.book_id = bd.id
        JOIN book_author ba ON bd.author_id = ba.id
        WHERE tb.user_id = $user_id
        GROUP BY bd.id, bd.title, ba.author, bd.cover_image
        ORDER BY borrow_date DESC
        LIMIT 3
    ");

        // var_dump($query->result_array());
        // die;
        return $query->result_array();
    }
}

<h1 align="center">Sistem Basis Data - E-PERPUS</h1>
<img src="./assets/img/metadata/E-PERPUS.png" alt="E-PERPUS">

## Daftar Isi

- [Pendahuluan](#pendahuluan)
- [Teknologi Yang Digunakan](#teknologi-yang-digunakan)
- [Dependency](#dependency)
- [Cara Install](#cara-install)
- [Menjalankan Program](#menjalankan-program)
  - [List User Login](#list-user-login)
  - [Tampilan Admin](#tampilan-admin)
  - [Tampilan User](#tampilan-user)
- [ERD (Entity Relationship Diagram)](#erd-entity-relationship-diagram)

## Pendahuluan

Repository ini merupakan Tugas Besar Untuk UAS dari mata kuliah Sistem Basis Data, tugas ini merupakan hasil dari kelompok yang dimana anggota nya adalah sebagai berikut:

- Adi Sani Alviga
- [Arman Dwi Pangestu](https://gitlab.com/armandwipangestu)
- Diaz Fadilah
- Vito Arsy Saputra

## Teknologi Yang Digunakan

Aplikasi ini dibuat menggunakan beberapa teknologi, diantaranya adalah:

> **Catatan**:
>
> - `Codeigniter` disini berfungsi sebagai framework backend
>
> - `Mazer` berfungsi sebagai template dari framework frontend yaitu `Bootstrap`

- [Codeigniter Versi 3.1.13](https://codeigniter.com/userguide3/installation/downloads.html)
- [Mazer Versi 2.3.0](https://github.com/zuramai/mazer/releases/tag/v2.3.0)
- [Bootstrap Versi 5.3.2](https://getbootstrap.com/)
- [PHP Dotenv for Codeigniter](https://github.com/agungjk/phpdotenv-for-codeigniter)

## Dependency

> **Catatan**:
>
> - Untuk PHP, MySQL dan Apache bisa di install dengan bundle seperti `XAMPP`, `MAMP`, `LAMP` atau `Laragon`
>
> - `Git` berfungsi untuk melakukan clone atau mendownload repository ini

- [PHP 5 ~ 8.0](https://www.php.net/releases/8.0/en.php)
- [MySQL 5.1+](https://downloads.mysql.com/archives/community/)
- [Apache](https://httpd.apache.org/)
- [Git](https://git-scm.com/downloads)

## Cara Install

<details open>
<summary><strong>Clone atau Download Repository ini</strong></summary>

> **Catatan**:
>
> Simpan folder e-perpus di lokasi `xampp/htdocs`, `/var/www`, `/srv/http` atau `laragon/www`

- Clone menggunakan SSH

```shell
git clone git@gitlab.com:sistem-basis-data/e-perpus.git
```

- Clone menggunakan HTTPS

```shell
git clone https://gitlab.com/sistem-basis-data/e-perpus.git
```

</details>

<details>
<summary><strong>Membuat file <code>.env.development</code></strong></summary>

```sh
cd e-perpus
```

```sh
cp .env.example .env.development
```

> **Catatan**:
>
> Sesuaikan isian .env.development dengan konfigurasi anda (seperti nama database dll)
>
> Default yang saya gunakan:
>
> ```
> DB_HOSTNAME=localhost
> DB_USERNAME=root
> DB_PASSWORD=
> DB_NAME=e-perpus
> DB_CONNECTION=mysqli
> BASE_URL=/e-perpus
> ```

</details>

<details>
<summary><strong>Menyesuaikan <code>base_url</code></strong></summary>

Anda bisa sesuaikan `base_url` nya pada file `/application/config/config.php`, default yang digunakan adalah seperti berikut ini:

```php
$config['base_url'] = 'http://' . $_SERVER['HTTP_HOST'] . getenv('BASE_URL');
```

</details>

<details>
<summary><strong>Import Database</strong></summary>

- Membuat database baru dengan nama `e-perpus`

  ![Create New Database](./assets/img/docs/create_database.png)

- Import `e-perpus/database/e-perpus.sql` ke dalam database melalui phpmyamdin

  ![Import Database](./assets/img/docs/import_database.png)

- Pilih Database

  ![Select Database](./assets/img/docs/select_database.png)

- Import Berhasil

  ![Import Success](./assets/img/docs/import_success.png)

</details>

## Menjalankan Program

Buka url `localhost/e-perpus` pada web browser yang digunakan, maka akan muncul tampilan login seperti berikut ini:

![E-PERPUS Login](./assets/img/docs/e-perpus_login.png)

Untuk login dapat membuat akun sendiri atau menggunakan akun berikut ini:

> **Catatan**: Default role akun yang dibuat baru adalah `Member`

### List User Login

| Email           | Password | Role          |
| --------------- | -------- | ------------- |
| admin@admin.com | 123      | Administrator |
| user@user.com   | 123      | Member        |

### Tampilan Admin

![Admin View](./assets/img/docs/admin-view.png)

### Tampilan User

![User View](./assets/img/docs/user-view.png)

## ERD (Entity Relationship Diagram)

![image](erd/ERD_SBD_TUGAS_BESAR_UAS.png)

Untuk melihat ERD dari program ini, kunjungi halaman berikut ini [whimsical.com/erd-sbd-tugas-besar-uas](https://whimsical.com/erd-sbd-tugas-besar-uas-YPg4kKz5Zj9sMfMYt7CCmL)

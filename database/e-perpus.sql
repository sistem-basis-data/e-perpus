-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 13, 2023 at 06:43 AM
-- Server version: 8.0.30
-- PHP Version: 8.0.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `e-perpus`
--

-- --------------------------------------------------------

--
-- Table structure for table `book_author`
--

CREATE TABLE `book_author` (
  `id` int NOT NULL,
  `author` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `book_author`
--

INSERT INTO `book_author` (`id`, `author`, `created_at`, `updated_at`) VALUES
(1, 'R. Sandhika Galih dan Acep Hendra', '2023-11-13 00:06:33', '2023-11-13 00:06:33'),
(2, 'George R. R. Martin', '2023-11-13 00:19:54', '2023-11-13 00:19:54'),
(3, 'J. K. Rowling', '2023-11-13 00:25:29', '2023-11-13 00:25:29');

-- --------------------------------------------------------

--
-- Table structure for table `book_data`
--

CREATE TABLE `book_data` (
  `id` int NOT NULL,
  `title` varchar(255) NOT NULL,
  `synopsis` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `language` varchar(255) NOT NULL,
  `publish_date` date NOT NULL,
  `total_page` int NOT NULL,
  `quantity_available` int NOT NULL,
  `cover_image` varchar(255) NOT NULL,
  `publisher_id` int NOT NULL,
  `author_id` int NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `book_data`
--

INSERT INTO `book_data` (`id`, `title`, `synopsis`, `language`, `publish_date`, `total_page`, `quantity_available`, `cover_image`, `publisher_id`, `author_id`, `created_at`, `updated_at`) VALUES
(1, 'DASAR-DASAR PEMROGRAMAN WEB', 'Website di era sekarang sudah menjadi kebutuhan utama yang tidak bisa diabaikan.\r\n\r\nSeluruh Sektor bisnis atau edukasi dapat memanfaatkan website sebagai alat untuk promosi, tukar informasi, dan lainnya.\r\n\r\nBerdasarkan data dari World Wide Web Technology Surveys, dari seluruh website yang aktif, 88.2% menggunakan HTML dan 95.6% menggunakan CSS.\r\n\r\nBuku ini, membahas tuntas mengenai HTML dan CSS sebagai fondasi dalam pembuatan website serta dilengkapi dengan Studi Kasus yang Relevan dan sesuai trend.', 'Indonesia', '2023-10-11', 414, 20, '6820aca47bd67_1699785893.webp', 1, 1, '2023-11-13 00:14:08', '2023-11-13 00:16:12'),
(2, 'A Game of Thrones', 'Here is the first volume in George R. R. Martin’s magnificent cycle of novels that includes A Clash of Kings and A Storm of Swords. As a whole, this series comprises a genuine masterpiece of modern fantasy, bringing together the best the genre has to offer. Magic, mystery, intrigue, romance, and adventure fill these pages and transport us to a world unlike any we have ever experienced. Already hailed as a classic, George R. R. Martin’s stunning series is destined to stand as one of the great achievements of imaginative fiction.\r\n\r\nA GAME OF THRONES\r\n\r\nLong ago, in a time forgotten, a preternatural event threw the seasons out of balance. In a land where summers can last decades and winters a lifetime, trouble is brewing. The cold is returning, and in the frozen wastes to the north of Winterfell, sinister and supernatural forces are massing beyond the kingdom’s protective Wall. At the center of the conflict lie the Starks of Winterfell, a family as harsh and unyielding as the land they were born to. Sweeping from a land of brutal cold to a distant summertime kingdom of epicurean plenty, here is a tale of lords and ladies, soldiers and sorcerers, assassins and bastards, who come together in a time of grim omens.\r\n\r\nHere an enigmatic band of warriors bear swords of no human metal; a tribe of fierce wildlings carry men off into madness; a cruel young dragon prince barters his sister to win back his throne; and a determined woman undertakes the most treacherous of journeys. Amid plots and counterplots, tragedy and betrayal, victory and terror, the fate of the Starks, their allies, and their enemies hangs perilously in the balance, as each endeavors to win that deadliest of conflicts: the game of thrones.', 'English', '2002-05-28', 704, 3, '0242ac120002_1699785893.jpg', 2, 2, '2023-11-13 00:24:07', '2023-11-13 00:24:07'),
(3, 'Harry Potter and the Deathly Hallows', 'It\'s no longer safe for Harry at Hogwarts, so he and his best friends, Ron and Hermione, are on the run. Professor Dumbledore has given them clues about what they need to do to defeat the dark wizard, Lord Voldemort, once and for all, but it\'s up to them to figure out what these hints and suggestions really mean. Their cross-country odyssey has them searching desperately for the answers, while evading capture or death at every turn. At the same time, their friendship, fortitude, and sense of right and wrong are tested in ways they never could have imagined. The ultimate battle between good and evil that closes out this final chapter of the epic series takes place where Harry\'s Wizarding life began: at Hogwarts. The satisfying conclusion offers shocking last-minute twists, incredible acts of courage, powerful new forms of magic, and the resolution of many mysteries. Above all, this intense, cathartic book serves as a clear statement of the message at the heart of the Harry Potter series: that choice matters much more than destiny, and that love will always triumph over death.', 'English', '2010-01-01', 759, 5, '5bbf6692_1699785893.jpg', 3, 3, '2023-11-13 00:27:21', '2023-11-13 00:27:21');

-- --------------------------------------------------------

--
-- Table structure for table `book_publisher`
--

CREATE TABLE `book_publisher` (
  `id` int NOT NULL,
  `publisher` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `book_publisher`
--

INSERT INTO `book_publisher` (`id`, `publisher`, `created_at`, `updated_at`) VALUES
(1, 'PT Inovasi Karya Mehendra', '2023-11-13 00:06:01', '2023-11-13 00:06:01'),
(2, 'Bantam Books', '2023-11-13 00:19:41', '2023-11-13 00:19:41'),
(3, 'Arthur A. Levine Books an imprint of Scholastic Inc.', '2023-11-13 00:25:15', '2023-11-13 00:25:15');

-- --------------------------------------------------------

--
-- Table structure for table `transaction_borrow`
--

CREATE TABLE `transaction_borrow` (
  `id` int NOT NULL,
  `borrow_date` date NOT NULL,
  `return_date` date NOT NULL,
  `quantity` int NOT NULL,
  `user_id` int NOT NULL,
  `book_id` int NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `transaction_borrow`
--

INSERT INTO `transaction_borrow` (`id`, `borrow_date`, `return_date`, `quantity`, `user_id`, `book_id`, `created_at`, `updated_at`) VALUES
(1, '2023-01-01', '2023-01-05', 1, 3, 1, '2023-11-13 00:44:50', '2023-11-13 03:00:34'),
(2, '2023-02-08', '2023-02-10', 1, 3, 3, '2023-11-13 00:49:49', '2023-11-13 03:00:41'),
(3, '2023-03-11', '2023-03-12', 1, 3, 2, '2023-11-13 00:50:32', '2023-11-13 03:00:51'),
(4, '2023-04-12', '2023-04-13', 1, 3, 1, '2023-11-13 02:58:17', '2023-11-13 02:58:17'),
(5, '2023-05-12', '2023-05-13', 1, 3, 1, '2023-11-13 03:39:50', '2023-11-13 03:39:50'),
(6, '2023-06-12', '2023-06-13', 1, 3, 2, '2023-11-13 03:39:50', '2023-11-13 03:39:50'),
(7, '2023-07-12', '2023-07-13', 1, 3, 3, '2023-11-13 03:39:50', '2023-11-13 03:39:50'),
(8, '2023-08-12', '2023-08-13', 1, 3, 1, '2023-11-13 03:39:50', '2023-11-13 03:39:50'),
(9, '2023-09-12', '2023-09-13', 1, 3, 2, '2023-11-13 03:39:50', '2023-11-13 03:39:50'),
(10, '2023-10-12', '2023-10-13', 1, 3, 3, '2023-11-13 03:39:50', '2023-11-13 03:39:50'),
(11, '2023-11-12', '2023-11-13', 1, 3, 1, '2023-11-13 03:39:50', '2023-11-13 03:39:50'),
(12, '2023-12-12', '2023-12-13', 1, 3, 2, '2023-11-13 03:39:50', '2023-11-13 03:39:50'),
(13, '2023-01-12', '2023-01-13', 1, 3, 1, '2023-11-13 03:39:50', '2023-11-13 03:39:50'),
(14, '2023-01-12', '2023-01-13', 1, 3, 1, '2023-11-13 03:39:50', '2023-11-13 03:39:50'),
(15, '2023-02-12', '2023-02-13', 1, 3, 2, '2023-11-13 03:39:50', '2023-11-13 03:39:50'),
(16, '2023-04-12', '2023-04-13', 1, 3, 3, '2023-11-13 03:39:50', '2023-11-13 03:39:50'),
(17, '2023-05-12', '2023-05-13', 1, 3, 3, '2023-11-13 03:39:50', '2023-11-13 03:39:50'),
(18, '2023-05-12', '2023-05-13', 1, 3, 2, '2023-11-13 03:39:50', '2023-11-13 03:39:50'),
(19, '2023-06-12', '2023-06-13', 1, 3, 1, '2023-11-13 03:39:50', '2023-11-13 03:39:50'),
(20, '2023-08-12', '2023-08-13', 1, 3, 1, '2023-11-13 03:39:50', '2023-11-13 03:39:50'),
(21, '2023-09-12', '2023-09-13', 1, 3, 2, '2023-11-13 03:39:50', '2023-11-13 03:39:50'),
(22, '2023-09-12', '2023-09-13', 1, 3, 2, '2023-11-13 03:39:50', '2023-11-13 03:39:50'),
(23, '2023-10-12', '2023-10-13', 1, 3, 3, '2023-11-13 03:39:50', '2023-11-13 03:39:50'),
(24, '2023-10-12', '2023-10-13', 1, 3, 3, '2023-11-13 03:39:50', '2023-11-13 03:39:50'),
(25, '2023-12-12', '2023-12-13', 1, 3, 1, '2023-11-13 03:39:50', '2023-11-13 03:39:50');

-- --------------------------------------------------------

--
-- Table structure for table `transaction_return`
--

CREATE TABLE `transaction_return` (
  `id` int NOT NULL,
  `return_date` date NOT NULL,
  `quantity` int NOT NULL,
  `fine_amount` decimal(10,0) NOT NULL,
  `borrow_id` int NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `transaction_return`
--

INSERT INTO `transaction_return` (`id`, `return_date`, `quantity`, `fine_amount`, `borrow_id`, `created_at`, `updated_at`) VALUES
(1, '2023-01-13', 1, 10000, 1, '2023-11-13 00:55:09', '2023-11-13 02:45:54'),
(2, '2023-02-10', 1, 0, 2, '2023-11-13 02:30:46', '2023-11-13 02:45:58'),
(3, '2023-03-05', 1, 0, 3, '2023-11-13 02:31:47', '2023-11-13 02:46:03'),
(4, '2023-04-14', 1, 5000, 4, '2023-11-13 03:04:09', '2023-11-13 03:04:09'),
(5, '2023-05-14', 1, 0, 5, '2023-11-13 03:04:09', '2023-11-13 03:04:09'),
(6, '2023-06-14', 1, 2500, 6, '2023-11-13 03:04:09', '2023-11-13 03:04:09'),
(7, '2023-07-14', 1, 0, 7, '2023-11-13 03:04:09', '2023-11-13 03:04:09'),
(8, '2023-08-14', 1, 0, 8, '2023-11-13 03:04:09', '2023-11-13 03:04:09'),
(9, '2023-09-14', 1, 0, 9, '2023-11-13 03:04:09', '2023-11-13 03:04:09'),
(10, '2023-10-14', 1, 0, 10, '2023-11-13 03:04:09', '2023-11-13 03:04:09'),
(11, '2023-11-14', 1, 0, 11, '2023-11-13 03:04:09', '2023-11-13 03:04:09'),
(12, '2023-12-14', 1, 0, 12, '2023-11-13 03:04:09', '2023-11-13 03:04:09'),
(13, '2023-01-14', 1, 0, 13, '2023-11-13 03:04:09', '2023-11-13 03:04:09'),
(14, '2023-01-14', 1, 0, 14, '2023-11-13 03:04:09', '2023-11-13 03:04:09'),
(15, '2023-02-14', 1, 0, 15, '2023-11-13 03:04:09', '2023-11-13 03:04:09'),
(16, '2023-04-14', 1, 0, 16, '2023-11-13 03:04:09', '2023-11-13 03:04:09'),
(17, '2023-05-14', 1, 0, 17, '2023-11-13 03:04:09', '2023-11-13 03:04:09'),
(18, '2023-05-14', 1, 0, 18, '2023-11-13 03:04:09', '2023-11-13 03:04:09'),
(19, '2023-06-14', 1, 0, 19, '2023-11-13 03:04:09', '2023-11-13 03:04:09'),
(20, '2023-08-14', 1, 0, 20, '2023-11-13 03:04:09', '2023-11-13 03:04:09'),
(21, '2023-09-14', 1, 0, 21, '2023-11-13 03:04:09', '2023-11-13 03:04:09'),
(22, '2023-09-14', 1, 0, 22, '2023-11-13 03:04:09', '2023-11-13 03:04:09'),
(23, '2023-10-14', 1, 0, 23, '2023-11-13 03:04:09', '2023-11-13 03:04:09'),
(24, '2023-10-14', 1, 0, 24, '2023-11-13 03:04:09', '2023-11-13 03:04:09'),
(25, '2023-12-14', 1, 0, 25, '2023-11-13 03:04:09', '2023-11-13 03:04:09');

-- --------------------------------------------------------

--
-- Table structure for table `user_access_menu`
--

CREATE TABLE `user_access_menu` (
  `id` int NOT NULL,
  `role_id` int NOT NULL,
  `menu_id` int NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `user_access_menu`
--

INSERT INTO `user_access_menu` (`id`, `role_id`, `menu_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2023-11-12 03:49:40', '2023-11-12 03:49:40'),
(2, 1, 2, '2023-11-12 03:49:40', '2023-11-12 03:49:40'),
(3, 1, 3, '2023-11-12 03:49:40', '2023-11-12 03:49:40'),
(4, 1, 4, '2023-11-12 03:49:40', '2023-11-12 03:49:40'),
(5, 2, 2, '2023-11-12 06:55:23', '2023-11-12 06:55:23'),
(6, 2, 5, '2023-11-13 00:35:19', '2023-11-13 00:35:19');

-- --------------------------------------------------------

--
-- Table structure for table `user_data`
--

CREATE TABLE `user_data` (
  `id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `gender` char(1) NOT NULL,
  `address` varchar(255) NOT NULL,
  `phone_number` varchar(13) NOT NULL,
  `avatar_image` varchar(255) NOT NULL,
  `role_id` int NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `user_data`
--

INSERT INTO `user_data` (`id`, `name`, `email`, `password`, `gender`, `address`, `phone_number`, `avatar_image`, `role_id`, `created_at`, `updated_at`) VALUES
(1, 'Arman Dwi Pangestu', 'armandwi.pangestu7@gmail.com', '$2y$10$sV2EyFOz5cdQOiYUkM.tGe6EjxJeYOkcRDEnuAs.ROfbUu/UtI1.G', 'L', 'Jl. Jakarta, Gg. Laksana 8 No. 192 RT. 09, RW. 03, Kecamatan Batununggal, Kelurahan Kebonwaru, Kota Bandung', '089637369606', '6550aca52bd67_1699785893.png', 1, '2023-01-11 23:26:37', '2023-11-12 14:16:48'),
(2, 'Administrator', 'admin@admin.com', '$2y$10$jyS2Wu/tEELyHgiHfpfBye5JqT8eyvUx3zBKSU1Sg7DE/.34FGFWS', 'L', 'Jl. Admin', '08123456789', 'default_laki_laki.png', 1, '2023-02-12 03:13:26', '2023-11-12 23:08:08'),
(3, 'User Testing', 'user@user.com', '$2y$10$YpQb0e1KrTZYwM6Sb7M86ePcritFnTWW6YpbRhw2Et29Asd6MAoKq', 'L', 'Jl Otto Iskandardinata 588 B, Jawa Barat', '0987654321', 'default_laki_laki.png', 2, '2023-03-12 03:13:44', '2023-11-13 06:30:32'),
(4, 'Admin Perempuan', 'adminp@adminp.com', '$2y$10$Q5c/lzex0JIuwPa6GyRMWexvlv.Yf7aRCSnCavwAsODB6LYemL09W', 'P', 'Jl. Admin Perempuan', '08123498765', 'default_perempuan.png', 1, '2023-04-12 03:36:19', '2023-11-12 14:17:01'),
(5, 'User Perempuan', 'userp@userp.com', '$2y$10$K5tWCXZz/zTW4Wa1LGYf4.U1o7Oolw20IFInvVLF3NU6vse4zm7.K', 'P', 'Jl. User Perempuan', '08123459812', 'default_perempuan.png', 2, '2023-05-12 06:56:48', '2023-11-12 14:17:05'),
(6, 'User1', 'user1@user1.com', '$2y$10$DD3vRCcNjTb/G.bLKBkjXen36C5WnNacFHSSVu.2p5izOu9Ptd5Ry', 'P', 'Jl. User1', '0123123', 'default_perempuan.png', 2, '2023-06-12 14:17:33', '2023-11-12 14:18:44'),
(7, 'User2', 'user2@user2.com', '$2y$10$2hE7kbVTdVzMj12SuWdDBe6ghYoPqDu91mySG3tCU7FywEluY3maW', 'P', 'Jl. User2', '01231231', 'default_perempuan.png', 2, '2023-07-12 14:17:56', '2023-11-12 14:18:48'),
(8, 'User3', 'user3@user3.com', '$2y$10$d2MPIx0AwutEeBveYbPA.udkSe46PoEZErV83SffyF0/PaOV5vRsq', 'P', 'Jl. User3', '0123125', 'default_perempuan.png', 2, '2023-08-12 14:18:14', '2023-11-12 14:18:53'),
(9, 'User4', 'user4@user4.com', '$2y$10$kWMj9H3dYNHaufM8Tr993.zhrWjhz1s4UNmmIPtpkvTgMkT3TIJ8a', 'P', 'Jl. User4', '01238124', 'default_perempuan.png', 2, '2023-09-12 14:18:34', '2023-11-12 14:18:56'),
(10, 'User5', 'user5@user5.com', '$2y$10$qPlGQmX941gyNaAUI/1bcep2ke/3QPLZKSX/rqlFVTuCXLUT87/De', 'P', 'Jl. User5', '012431254', 'default_perempuan.png', 2, '2023-10-12 14:19:14', '2023-11-12 14:20:05'),
(11, 'User6', 'user6@user6.com', '$2y$10$qxqUbAE9bkPzf3CVkUANjut/2n3PS7wlh5MResZgZmbo94FQYnjHG', 'P', 'Jl. User6', '0123123', 'default_perempuan.png', 2, '2023-11-12 14:19:38', '2023-11-12 14:19:38'),
(12, 'User7', 'user7@user7.com', '$2y$10$q2DHEZlgQtYbe.WApj1U6eC/ahRiQngUqGP5cEyONPN0YtU280NnK', 'P', 'Jl. User7', '01283123', 'default_perempuan.png', 2, '2023-12-12 14:19:58', '2023-11-12 14:20:12'),
(13, 'User8', 'user8@user8.com', '$2y$10$k/nIXb9ixx0qtKggYvUOQ.Ou3zwCODo1WwFWpBn3TPdmlrXsH1CcO', 'L', 'Jl. User8', '0123213', 'default_laki_laki.png', 2, '2023-02-12 14:21:59', '2023-11-12 14:25:02'),
(14, 'User9', 'user9@user9.com', '$2y$10$JegSYI3UxF0AYJsvA5qCXOg.0q8NZNp4ffiuqMjVa6qlK0H.pvdIS', 'L', 'Jl. User9', '08766123', 'default_laki_laki.png', 2, '2023-03-12 14:22:21', '2023-11-12 14:25:19'),
(15, 'User10', 'user10@user10.com', '$2y$10$Foynetr0Eq13d5rJKF2cR.GO5swhqb9.2L1z07N8XnSgICxpE3I36', 'L', 'Jl. User10', '0123176235', 'default_laki_laki.png', 2, '2023-03-12 14:24:44', '2023-11-12 14:25:27'),
(16, 'User11', 'user11@user11.com', '$2y$10$Foynetr0Eq13d5rJKF2cR.GO5swhqb9.2L1z07N8XnSgICxpE3I36', 'P', 'Jl. User11', '0123176235', 'default_perempuan.png', 2, '2023-04-12 14:24:44', '2023-11-12 14:25:27'),
(17, 'User12', 'user12@user12.com', '$2y$10$Foynetr0Eq13d5rJKF2cR.GO5swhqb9.2L1z07N8XnSgICxpE3I36', 'P', 'Jl. User12', '0123176235', 'default_perempuan.png', 2, '2023-06-12 14:24:44', '2023-11-12 14:25:27'),
(18, 'User13', 'user13@user13.com', '$2y$10$Foynetr0Eq13d5rJKF2cR.GO5swhqb9.2L1z07N8XnSgICxpE3I36', 'P', 'Jl. User13', '0123176235', 'default_perempuan.png', 2, '2023-07-12 14:24:44', '2023-11-12 14:25:27'),
(19, 'User14', 'user14@user14.com', '$2y$10$Foynetr0Eq13d5rJKF2cR.GO5swhqb9.2L1z07N8XnSgICxpE3I36', 'P', 'Jl. User14', '0123176235', 'default_perempuan.png', 2, '2023-07-12 14:24:44', '2023-11-12 14:25:27'),
(20, 'User15', 'user15@user15.com', '$2y$10$Foynetr0Eq13d5rJKF2cR.GO5swhqb9.2L1z07N8XnSgICxpE3I36', 'P', 'Jl. User15', '0123176235', 'default_perempuan.png', 2, '2023-08-12 14:24:44', '2023-11-12 14:25:27'),
(21, 'User16', 'user16@user16.com', '$2y$10$Foynetr0Eq13d5rJKF2cR.GO5swhqb9.2L1z07N8XnSgICxpE3I36', 'P', 'Jl. User16', '0123176235', 'default_perempuan.png', 2, '2023-08-12 14:24:44', '2023-11-12 14:25:27'),
(22, 'User17', 'user17@user17.com', '$2y$10$Foynetr0Eq13d5rJKF2cR.GO5swhqb9.2L1z07N8XnSgICxpE3I36', 'P', 'Jl. User17', '0123176235', 'default_perempuan.png', 2, '2023-10-12 14:24:44', '2023-11-12 14:25:27'),
(23, 'User18', 'user18@user18.com', '$2y$10$Foynetr0Eq13d5rJKF2cR.GO5swhqb9.2L1z07N8XnSgICxpE3I36', 'P', 'Jl. User18', '0123176235', 'default_perempuan.png', 2, '2023-11-12 14:24:44', '2023-11-12 14:25:27'),
(24, 'User19', 'user19@user19.com', '$2y$10$Foynetr0Eq13d5rJKF2cR.GO5swhqb9.2L1z07N8XnSgICxpE3I36', 'P', 'Jl. User19', '0123176235', 'default_perempuan.png', 2, '2023-11-12 14:24:44', '2023-11-12 14:25:27'),
(25, 'User20', 'user20@user20.com', '$2y$10$Foynetr0Eq13d5rJKF2cR.GO5swhqb9.2L1z07N8XnSgICxpE3I36', 'P', 'Jl. User20', '0123176235', 'default_perempuan.png', 2, '2023-12-12 14:24:44', '2023-11-12 14:25:27');

-- --------------------------------------------------------

--
-- Table structure for table `user_menu`
--

CREATE TABLE `user_menu` (
  `id` int NOT NULL,
  `menu` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `user_menu`
--

INSERT INTO `user_menu` (`id`, `menu`, `created_at`, `updated_at`) VALUES
(1, 'Admin', '2023-11-12 03:47:08', '2023-11-12 03:47:08'),
(2, 'User', '2023-11-12 03:47:08', '2023-11-12 03:47:08'),
(3, 'Menu', '2023-11-12 03:47:23', '2023-11-12 03:47:23'),
(4, 'Submenu', '2023-11-12 03:47:23', '2023-11-12 03:47:23'),
(5, 'Member', '2023-11-13 00:34:15', '2023-11-13 00:39:57');

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `id` int NOT NULL,
  `role` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`id`, `role`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', '2023-11-12 03:54:47', '2023-11-12 03:54:47'),
(2, 'Member', '2023-11-12 03:54:47', '2023-11-12 03:54:47');

-- --------------------------------------------------------

--
-- Table structure for table `user_sub_menu`
--

CREATE TABLE `user_sub_menu` (
  `id` int NOT NULL,
  `title` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `icon` varchar(255) NOT NULL,
  `menu_id` int NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `user_sub_menu`
--

INSERT INTO `user_sub_menu` (`id`, `title`, `url`, `icon`, `menu_id`, `created_at`, `updated_at`) VALUES
(1, 'Dashboard', 'admin', 'bi bi-grid-fill', 1, '2023-11-12 04:02:52', '2023-11-12 04:02:52'),
(2, 'Role Akses', 'admin/role', 'bi bi-person-badge', 1, '2023-11-12 04:02:52', '2023-11-12 04:04:49'),
(3, 'User Data', 'admin/user_data', 'bi bi-people', 1, '2023-11-12 04:36:19', '2023-11-12 04:36:19'),
(4, 'Profil Saya', 'user', 'bi bi-person-vcard', 2, '2023-11-12 04:38:53', '2023-11-12 04:38:53'),
(5, 'Ubah Profil', 'user/ubah', 'bi bi-person-gear', 2, '2023-11-12 04:38:53', '2023-11-12 04:38:53'),
(6, 'Ganti Kata Sandi', 'user/ganti_kata_sandi', 'bi bi-key', 2, '2023-11-12 04:40:15', '2023-11-12 04:40:15'),
(7, 'Menu Management', 'menu', 'bi bi-folder2', 3, '2023-11-12 04:41:56', '2023-11-12 04:41:56'),
(8, 'Submenu Management', 'submenu', 'bi bi-folder2-open', 4, '2023-11-12 04:41:56', '2023-11-12 04:41:56'),
(9, 'Dashboard', 'member', 'bi bi-grid-fill', 5, '2023-11-13 00:35:06', '2023-11-13 00:41:25');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `book_author`
--
ALTER TABLE `book_author`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_data`
--
ALTER TABLE `book_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_publisher`
--
ALTER TABLE `book_publisher`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaction_borrow`
--
ALTER TABLE `transaction_borrow`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaction_return`
--
ALTER TABLE `transaction_return`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_access_menu`
--
ALTER TABLE `user_access_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_data`
--
ALTER TABLE `user_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_menu`
--
ALTER TABLE `user_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_sub_menu`
--
ALTER TABLE `user_sub_menu`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `book_author`
--
ALTER TABLE `book_author`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `book_data`
--
ALTER TABLE `book_data`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `book_publisher`
--
ALTER TABLE `book_publisher`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `transaction_borrow`
--
ALTER TABLE `transaction_borrow`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `transaction_return`
--
ALTER TABLE `transaction_return`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `user_access_menu`
--
ALTER TABLE `user_access_menu`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `user_data`
--
ALTER TABLE `user_data`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `user_menu`
--
ALTER TABLE `user_menu`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_sub_menu`
--
ALTER TABLE `user_sub_menu`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
